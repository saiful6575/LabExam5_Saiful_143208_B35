<?php
class MyCalculator {
    private $_value1, $_value2;
    public function __construct( $value1, $value2 ) {
        $this->_value1 = $value1;
        $this->_value2= $value2;
    }
    public function add() {
        return $this->_value1 + $this->_value2;
    }

    public function multiply() {
        return $this->_value1 * $this->_value2;
    }
    public function subtract(){
        return $this->_value1 - $this->_value2;
    }
    public function Division(){
        return $this->_value1/$this->_value2;
    }

}
$mycalc = new MyCalculator(12, 6);
echo $mycalc-> add();
echo"<br>";
echo $mycalc-> multiply();
echo"<br>";
echo $mycalc-> subtract();
echo"<br>";
echo $mycalc->Division();
?>